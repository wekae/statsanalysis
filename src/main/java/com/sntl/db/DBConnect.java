/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sntl.db;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

/**
 *
 * @author solunet
 */
public class DBConnect {
    public static Connection getConnection(){
        Connection connection = null;
        
        Properties prop = new Properties();
        InputStream propsFile = null;
        
        try{
            propsFile = new FileInputStream("config.properties");
            prop.load(propsFile);
            
            Class.forName(prop.getProperty("DbDriverClass"));
            
            String url = prop.getProperty("DbUrl");
            String user = prop.getProperty("DbUser");
            String password = prop.getProperty("DbPass");
            
            connection = DriverManager.getConnection(url,user, password);
            
        }catch(IOException ioe){
            ioe.printStackTrace();
        }catch(ClassNotFoundException cne){
            cne.printStackTrace();
        }catch(SQLException se){
            se.printStackTrace();
        }
        
        return connection;
    }
}
