/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sntl.util;

/**
 *
 * @author victor w
 */

import com.sntl.models.RevenueStat;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.List;

import java.util.Base64;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Field;
import java.util.AbstractList;
import java.util.Base64.Encoder;

/**
 *
 * @author Victor W
 */
public class JSONConverter {
    public JSONConverter(){
        
    }
    
    public static String formatToJson(ResultSet rs)throws SQLException{
        String returnData="{\"results\":[{";
        Encoder encoder = Base64.getEncoder();
        
        ResultSetMetaData rsmd = rs.getMetaData();
        int columnCount = rsmd.getColumnCount();
        System.out.println("#########"+columnCount);
        while(rs.next()){            
            for(int i=1; i<=columnCount;i++){
                String innerElement = "";
                System.out.println(i);
                System.out.println(rs.getString(i));
                try{
                    if(i==1 && columnCount ==1){
                        innerElement+="\""+rsmd.getColumnLabel(i)+"\":\"" + encoder.encode(rs.getString(i).getBytes("UTF-8"))+"\"}";
                    }else if (i==1 && columnCount >1){
                        innerElement+="\""+rsmd.getColumnLabel(i)+"\":\"" + encoder.encode(rs.getString(i).getBytes("UTF-8"))+"\",";
                    }else if (i==columnCount){
                        innerElement+="\""+rsmd.getColumnLabel(i)+"\":\"" + encoder.encode(rs.getString(i).getBytes("UTF-8"))+"\"}";
                    }else if(i>1 && i<columnCount){                    
                        innerElement+="\""+rsmd.getColumnLabel(i)+"\":\"" + encoder.encode(rs.getString(i).getBytes("UTF-8"))+"\",";
                    }       
                }catch(UnsupportedEncodingException uee){
                    uee.printStackTrace();
                }
                
    //                circuit+="}";
                returnData+=innerElement;
            }    
            if(!rs.isLast()){
                        returnData+=",{";
            }
        }
        returnData+="]}";
        
        
        
        
        
        return returnData;      
        
    }
    
    public static String formatToJSON(Hashtable<String, Object> ht)throws IllegalAccessException{
        
        String converted="{";
        
        Enumeration eKeys = ht.keys();
        final int items = ht.size();
        String[] keys = getKeys(eKeys, items);
        
        
        
        for(int i=0; i<ht.size(); i++){
            String key = keys[i];
//            System.out.println("KEY: "+key);
            
            
            String value = getValue(ht.get(key));
            System.out.println("CLASS:  "+ht.get(key).getClass());
            
            String element = "\""+key+"\":"+value;
            
            
            System.out.println("KEY: "+key+" | "+"Value: "+value);
            System.out.println();
            if(i<(ht.size()-1)){
                element += ",";
            }
            converted += element; 
            
        }
        
        
        converted +="}";
        
        return converted;
        
        
        
    }
    
    public static String formatToJSON(List list)throws IllegalAccessException{
        String returnData="{\"results\":[";
        
        final int count = list.size();
        int i = 0;
        for(Object obj: list){
            String element = getValue(obj);            
            
            
            if(i<count-1){
                element += ",";
            }
            returnData += element;
            
            i++;
                        
        }
        
        
        
        
        returnData+="]}";        
        
        return returnData;
    }
    
    public static String formatToJSON(Object object)throws IllegalAccessException{
        String returnData="{";
        
        Class<?> objectClass = object.getClass();
        Field[] fields = objectClass.getDeclaredFields();
        
        final int count = fields.length;
        int i = 0;
        
        for(Field field:fields){
//            System.out.println("COUNT: "+i);
//            System.out.println("NAME: "+field.getName()+" VALUE: "+field.get(object));
            if(i<count-1){
                returnData+="\""+field.getName()+"\":"+formatValue(field.get(object))+",";
            }else{
                returnData+="\""+field.getName()+"\":"+formatValue(field.get(object));
            }
            i++;
        }
        
        
        
        
        returnData+="}";        
        
        return returnData;
    }
    
    
    
    
    
    private static String[] getKeys(Enumeration eKeys, final int count){
        String[] keys = new String[count];
        
        int i=0;
        
        while(eKeys.hasMoreElements()){
            keys[i] = (String)eKeys.nextElement();
            i++;
        }
        
        return keys;        
        
    }
    
    private static String getValue(Object objectValue)throws IllegalAccessException{
        String value = "";
        
        if(objectValue.getClass().isArray()){
            
                value = formatArray(objectValue);
                
                System.out.println("Here");
                
        }else{
            if(objectValue.getClass().equals(Integer.class) || objectValue.getClass().equals(String.class) || objectValue.getClass().equals(Double.class)){
                //handle int, string , double (primitives)
                value = "\""+objectValue+"\"";
            }else if(objectValue.getClass().equals(Hashtable.class)){
                //handle hasttable
                value = ""+formatToJSON((Hashtable)objectValue)+"";

            }else if(objectValue.getClass().equals(ArrayList.class)){
                value = formatToJSON((List)objectValue);
                
            }else{
                value = formatToJSON(objectValue);
            }
//               if(objectValue.getClass().equals(RevenueStat.class))              
        }
        
        
        return value;
    }
    
    private static String formatArray(Object hashTableObject){
        
        String formatted = "[";
        
        if(hashTableObject.equals(int[].class)){
            System.out.println("TRUE:  ");
        }else{
            System.out.println("FALSE:    "+hashTableObject.getClass());
        }
        
        
        Object[] arrayObj = (Object[])hashTableObject;
        int count = arrayObj.length;
        System.out.println("LENGTH:   "+count);
        
        int i=0;
        for(Object obj: arrayObj){         
               
            formatted +="\""+obj+"\"";
            
            
            if(i<count-1){
                formatted +=",";
            }
            
            i++;
        }
        
        formatted += "]";
        return formatted;
    }
    
    public static Object formatValue(Object value){
        Object returnValue = "";
        if(value.getClass().equals(Integer.class)|| value.getClass().equals(Double.class)|| value.getClass().equals(Long.class)|| value.getClass().equals(Float.class)|| value.getClass().equals(Short.class)){
            returnValue = value;
        }else if(value.getClass().equals(String.class)){
            returnValue = "\""+value+"\"";
        }
        
        return returnValue;
                
    }
    
    

    public static void main(String[] args) {
        // TODO code application logic here
        
        Hashtable ht = new Hashtable();
        ht.put("key1", 1);
        
        ht.put("key2", "TWO");
        
//        int[] three = {3,3,3};
//        ht.put("key3", three);
//        
//        double[] eight = {8.8,8.8,8.8,8.8,8.8,8.8,8.8,8.8};
//        ht.put("key8", eight);
        
        String[] nine = {"3","3","3"};
        ht.put("key9", nine);
        
        String[] ten = {"8.8","8.8","8.8","8.8","8.8","8.8","8.8","8.8"};
        ht.put("key10", ten);
        
        Hashtable ht2 = new Hashtable();
        ht2.put("key5", 5);
        
        ht2.put("key6", "SIX");
        
        String[] seven = {"SEVEN","Seven","seven"};
        ht2.put("key7", seven);
        
        Hashtable ht3 = new Hashtable();
        ht3.put("ht1", ht);
        ht3.put("ht2", ht2);
        
        
        
        Hashtable person = new Hashtable();
        person.put("firstName", "Victor");
        person.put("lastName", "W");
        person.put("age", "100");
        person.put("weight", "200");
        person.put("height", "Tall");
        person.put("address", "32333 APlace");
        person.put("phone", "0704333432");
        
        
        List<Hashtable> peopleList = new ArrayList();
        peopleList.add(person);
        peopleList.add(person);
        peopleList.add(person);
        peopleList.add(person);
        peopleList.add(person);
        peopleList.add(person);
        
        
        List alphabet = new ArrayList();
        alphabet.add("A");
        alphabet.add("B");
        alphabet.add("C");
        alphabet.add("D");
        alphabet.add("E");
        alphabet.add("F");
        alphabet.add("G");
        alphabet.add("H");
        alphabet.add("I");
        alphabet.add("J");
        alphabet.add("K");
        alphabet.add("L");
        
        List<RevenueStat> statistics = new ArrayList<RevenueStat>();
        RevenueStat one = new RevenueStat();
        one.setDate("01/12/2017");
        one.setCpID("601396");
        one.setCpName("SolunetMn");
        one.setServiceID("6.01E+15");
        one.setServiceContentName("22822_Breaking_News");
        one.setAccessCode("22822");
        one.setProductID("MDSP2000061748");
        one.setProductName("22822_Breaking_News");
        one.setServiceType("sms");
        one.setFee(0L);
        one.setQuantity(32L);
        one.setTotalRevenue(0L);
        
        RevenueStat two = new RevenueStat();
        two.setDate("01/12/2017");
        two.setCpID("601396");
        two.setCpName("SolunetMn");
        two.setServiceID("6.01E+15");
        two.setServiceContentName("22822_Love_Tips");
        two.setAccessCode("22822");
        two.setProductID("MDSP2000062938");
        two.setProductName("22822_Love_Tips");
        two.setServiceType("sms");
        two.setFee(10L);
        two.setQuantity(36L);
        two.setTotalRevenue(360L);				
        
        RevenueStat three = new RevenueStat();
        three.setDate("01/12/2017");
        three.setCpID("601396");
        three.setCpName("SolunetMn");
        three.setServiceID("6.01E+15");
        three.setServiceContentName("22454_Gospel");
        three.setAccessCode("22454");
        three.setProductID("MDSP2000067513");
        three.setProductName("22454_Gospel_Yesu_Iroma");
        three.setServiceType("sms");
        three.setFee(-25L);
        three.setQuantity(2268L);
        three.setTotalRevenue(-56700L);				


        
        statistics.add(one);
        statistics.add(two);
        statistics.add(three);
        

        List mix = new ArrayList();
        mix.add(peopleList);
        mix.add(alphabet);
        mix.add(ht3);
        mix.add(statistics);
        
        try{
            String converted = formatToJSON(mix);
            System.out.println(converted);
        }catch(IllegalAccessException iae){
            System.out.println(iae.toString());
        }
    }
    
    
}
