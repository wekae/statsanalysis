/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sntl.core;

import com.sntl.exceptions.NoResultsException;
import com.sntl.filters.FilterII;
import com.sntl.models.RevenueStat;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

/**
 *
 * @author solunet
 */
public class Analysis {
    private FilterII filter;
    private FilterII deliveriesFilter;
    private FilterII reversalsFilter;
    
    
    public static enum Headers{
        Date, 
        ServiceID, 
        ServiceContentName, 
        AccessCode, 
        ProductID, 
        ProductName 
    }
    
    public Analysis(String file)throws FileNotFoundException,IOException{
        this.filter = new FilterII(file);
        this.deliveriesFilter = new FilterII(file).getDeliveries();
        this.reversalsFilter = new FilterII(file).getReversals();
    }
    
    public Analysis(File file)throws FileNotFoundException,IOException{
        this.filter = new FilterII(file);
        this.deliveriesFilter = new FilterII(file).getDeliveries();
        this.reversalsFilter = new FilterII(file).getReversals();
    }
    
    
    public HashMap getCummulativeSummary()throws IOException{
        HashMap summary = new HashMap();
        
        List<RevenueStat> deliveries22454 = this.filter.getDeliveries().byAccessCode("22454").list();
        List<RevenueStat> reversals22454 = this.filter.getReversals().byAccessCode("22454").list();
        
        List<RevenueStat> deliveries22455 = this.filter.getDeliveries().byAccessCode("22455").list();
        List<RevenueStat> reversals22455 = this.filter.getReversals().byAccessCode("22455").list();
        
        List<RevenueStat> deliveries22822 = this.filter.getDeliveries().byAccessCode("22822").list();
        List<RevenueStat> reversals22822 = this.filter.getReversals().byAccessCode("22822").list();
        
        List<RevenueStat> deliveries22899 = this.filter.getDeliveries().byAccessCode("22899").list();
        List<RevenueStat> reversals22899 = this.filter.getReversals().byAccessCode("22899").list();
        
        List<RevenueStat> deliveries20303 = this.filter.getDeliveries().byAccessCode("20303").list();
        List<RevenueStat> reversals20303 = this.filter.getReversals().byAccessCode("20303").list();
            
        
        Object[] stat22454Deliveries= {"22454", this.filter.getTotalQuantity(deliveries22454), this.filter.getTotalRevenue(deliveries22454)};
        Object[] stat22454Reversals= {"22454", this.filter.getTotalQuantity(reversals22454), this.filter.getTotalRevenue(reversals22454)};
        
        
        Object[] stat22455Deliveries= {"22455", this.filter.getTotalQuantity(deliveries22455), this.filter.getTotalRevenue(deliveries22455)};
        Object[] stat22455Reversals= {"22455", this.filter.getTotalQuantity(reversals22455), this.filter.getTotalRevenue(reversals22455)};
        
        
        Object[] stat22822Deliveries= {"22822", this.filter.getTotalQuantity(deliveries22822), this.filter.getTotalRevenue(deliveries22822)};
        Object[] stat22822Reversals= {"22822", this.filter.getTotalQuantity(reversals22822), this.filter.getTotalRevenue(reversals22822)};
        
        
        Object[] stat22899Deliveries= {"22899", this.filter.getTotalQuantity(deliveries22899), this.filter.getTotalRevenue(deliveries22899)};
        Object[] stat22899Reversals= {"22899", this.filter.getTotalQuantity(reversals22899), this.filter.getTotalRevenue(reversals22899)};
        
        
        Object[] stat20303Deliveries= {"20303", this.filter.getTotalQuantity(deliveries20303), this.filter.getTotalRevenue(deliveries20303)};
        Object[] stat20303Reversals= {"20303", this.filter.getTotalQuantity(reversals20303), this.filter.getTotalRevenue(reversals20303)};
        
        List<RevenueStat> deliveries = this.filter.getDeliveries().list();
        List<RevenueStat> reversals = this.filter.getReversals().list();
        
        long deliveryQuantity = this.filter.getTotalQuantity(deliveries);
        long deliveryRevenue = this.filter.getTotalRevenue(deliveries);
        long reversalQuantity = this.filter.getTotalQuantity(reversals);
        long reversalRevenue = this.filter.getTotalRevenue(reversals);
        
        Object[] statDeliveries= {"TOTAL", deliveryQuantity, deliveryRevenue};
        Object[] statReversals= {"TOTAL", reversalQuantity, reversalRevenue};
        
        List<Object[]> deliveryList = new ArrayList<Object[]>();
        List<Object[]> reversalList = new ArrayList<Object[]>();
        
        deliveryList.add(stat22454Deliveries);
        deliveryList.add(stat22455Deliveries);
        deliveryList.add(stat22822Deliveries);
        deliveryList.add(stat22899Deliveries);
        deliveryList.add(stat20303Deliveries);
        deliveryList.add(statDeliveries);
        
        
        reversalList.add(stat22454Reversals);
        reversalList.add(stat22455Reversals);
        reversalList.add(stat22822Reversals);
        reversalList.add(stat22899Reversals);
        reversalList.add(stat20303Reversals);
        reversalList.add(statReversals);
        
        String startDate = this.filter.getStartDate(deliveries);
        String endDate = this.filter.getEndDate(deliveries);
        
        summary.put("deliveries", deliveryList);
        summary.put("reversals", reversalList);
        summary.put("deliveryQuantity", deliveryQuantity);
        summary.put("deliveryRevenue", deliveryRevenue);
        summary.put("reversalQuantity", reversalQuantity);
        summary.put("reversalRevenue", reversalRevenue);
        summary.put("startDate", startDate);
        summary.put("endDate", endDate);
        
        return summary;
    }
    
    public HashMap getSummaryByDate(String date)throws FileNotFoundException, IOException{
        HashMap summary = new HashMap();
        
        List<RevenueStat> deliveries22454 = this.filter.getDeliveries().byAccessCode("22454").byDate(date).list();
        List<RevenueStat> reversals22454 = this.filter.getReversals().byAccessCode("22454").byDate(date).list();
        
        List<RevenueStat> deliveries22455 = this.filter.getDeliveries().byAccessCode("22455").byDate(date).list();
        List<RevenueStat> reversals22455 = this.filter.getReversals().byAccessCode("22455").byDate(date).list();
        
        List<RevenueStat> deliveries22822 = this.filter.getDeliveries().byAccessCode("22822").byDate(date).list();
        List<RevenueStat> reversals22822 = this.filter.getReversals().byAccessCode("22822").byDate(date).list();
        
        List<RevenueStat> deliveries22899 = this.filter.getDeliveries().byAccessCode("22899").byDate(date).list();
        List<RevenueStat> reversals22899 = this.filter.getReversals().byAccessCode("22899").byDate(date).list();
        
        List<RevenueStat> deliveries20303 = this.filter.getDeliveries().byAccessCode("20303").byDate(date).list();
        List<RevenueStat> reversals20303 = this.filter.getReversals().byAccessCode("20303").byDate(date).list();
            
        
        Object[] stat22454Deliveries= {"22454", this.filter.getTotalQuantity(deliveries22454), this.filter.getTotalRevenue(deliveries22454)};
        Object[] stat22454Reversals= {"22454", this.filter.getTotalQuantity(reversals22454), this.filter.getTotalRevenue(reversals22454)};
        
        
        Object[] stat22455Deliveries= {"22455", this.filter.getTotalQuantity(deliveries22455), this.filter.getTotalRevenue(deliveries22455)};
        Object[] stat22455Reversals= {"22455", this.filter.getTotalQuantity(reversals22455), this.filter.getTotalRevenue(reversals22455)};
        
        
        Object[] stat22822Deliveries= {"22822", this.filter.getTotalQuantity(deliveries22822), this.filter.getTotalRevenue(deliveries22822)};
        Object[] stat22822Reversals= {"22822", this.filter.getTotalQuantity(reversals22822), this.filter.getTotalRevenue(reversals22822)};
        
        
        Object[] stat22899Deliveries= {"22899", this.filter.getTotalQuantity(deliveries22899), this.filter.getTotalRevenue(deliveries22899)};
        Object[] stat22899Reversals= {"22899", this.filter.getTotalQuantity(reversals22899), this.filter.getTotalRevenue(reversals22899)};
        
        
        Object[] stat20303Deliveries= {"20303", this.filter.getTotalQuantity(deliveries20303), this.filter.getTotalRevenue(deliveries20303)};
        Object[] stat20303Reversals= {"20303", this.filter.getTotalQuantity(reversals20303), this.filter.getTotalRevenue(reversals20303)};
        
        List<RevenueStat> deliveries = this.filter.getDeliveries().byDate(date).list();
        List<RevenueStat> reversals = this.filter.getReversals().byDate(date).list();
        
        long deliveryQuantity = this.filter.getTotalQuantity(deliveries);
        long deliveryRevenue = this.filter.getTotalRevenue(deliveries);
        long reversalQuantity = this.filter.getTotalQuantity(reversals);
        long reversalRevenue = this.filter.getTotalRevenue(reversals);
        
        Object[] statDeliveries= {"TOTAL", deliveryQuantity, deliveryRevenue};
        Object[] statReversals= {"TOTAL", reversalQuantity, reversalRevenue};
        
        List<Object[]> deliveryList = new ArrayList<Object[]>();
        List<Object[]> reversalList = new ArrayList<Object[]>();
        
        deliveryList.add(stat22454Deliveries);
        deliveryList.add(stat22455Deliveries);
        deliveryList.add(stat22822Deliveries);
        deliveryList.add(stat22899Deliveries);
        deliveryList.add(stat20303Deliveries);
        deliveryList.add(statDeliveries);
        
        
        reversalList.add(stat22454Reversals);
        reversalList.add(stat22455Reversals);
        reversalList.add(stat22822Reversals);
        reversalList.add(stat22899Reversals);
        reversalList.add(stat20303Reversals);
        reversalList.add(statReversals);
        
        String startDate = this.filter.getStartDate(deliveries);
        String endDate = this.filter.getEndDate(deliveries);
        
        summary.put("deliveries", deliveryList);
        summary.put("reversals", reversalList);
        summary.put("deliveryQuantity", deliveryQuantity);
        summary.put("deliveryRevenue", deliveryRevenue);
        summary.put("reversalQuantity", reversalQuantity);
        summary.put("reversalRevenue", reversalRevenue);
        summary.put("startDate", startDate);
        summary.put("endDate", endDate);
        
        return summary;
    }
    
    public HashMap getSummaryBy(Headers header, String criteria)throws FileNotFoundException, IOException{
        HashMap summary = new HashMap();
        List<RevenueStat> deliveries = new ArrayList<RevenueStat>();
        List<RevenueStat> reversals = new ArrayList<RevenueStat>();
        
        switch(header){
            case AccessCode:
                deliveries = this.filter.getDeliveries().byAccessCode(criteria).list();
                reversals = this.filter.getReversals().byAccessCode(criteria).list();
                break;
            case Date:
                deliveries = this.filter.getDeliveries().byDate(criteria).list();
                reversals = this.filter.getReversals().byDate(criteria).list();                
                break;
            case ProductID:
                deliveries = this.filter.getDeliveries().byProductID(criteria).list();
                reversals = this.filter.getReversals().byProductID(criteria).list();                
                break;
            case ProductName:
                deliveries = this.filter.getDeliveries().byProductName(criteria).list();
                reversals = this.filter.getReversals().byProductName(criteria).list();                
                break;
            case ServiceContentName:
                deliveries = this.filter.getDeliveries().byServiceContentName(criteria).list();
                reversals = this.filter.getReversals().byServiceContentName(criteria).list();                
                break;
            case ServiceID:
                deliveries = this.filter.getDeliveries().byServiceID(criteria).list();
                reversals = this.filter.getReversals().byServiceID(criteria).list();                
                break;
            default:
                deliveries = this.filter.getDeliveries().list();
                reversals = this.filter.getReversals().list();                   
                break;
        }
        
        List<Object[]> deliveryList = new ArrayList<Object[]>();
        List<Object[]> reversalList = new ArrayList<Object[]>();
        
        for(RevenueStat delivery: deliveries){
            Object[] statDelivery= 
                {
                    delivery.getDate(), 
                    delivery.getProductID(), 
                    delivery.getProductName(),
                    delivery.getAccessCode(),
                    delivery.getServiceContentName(),
                    delivery.getServiceID(),
                    delivery.getFee(),
                    delivery.getQuantity(),
                    delivery.getTotalRevenue()
                };
            
            deliveryList.add(statDelivery);
        }
        
        for(RevenueStat reversal: reversals){
            Object[] statReversal= 
                {
                    reversal.getDate(), 
                    reversal.getProductID(), 
                    reversal.getProductName(),
                    reversal.getAccessCode(),
                    reversal.getServiceContentName(),
                    reversal.getServiceID(),
                    reversal.getFee(),
                    reversal.getQuantity(),
                    reversal.getTotalRevenue()
                };
            
            reversalList.add(statReversal);
        }
        
        
        long deliveryQuantity = this.filter.getTotalQuantity(deliveries);
        long deliveryRevenue = this.filter.getTotalRevenue(deliveries);
        long reversalQuantity = this.filter.getTotalQuantity(reversals);
        long reversalRevenue = this.filter.getTotalRevenue(reversals);
        
        Object[] statDeliveries= {"TOTAL", "QUANTITY", deliveryQuantity, "REVENUE", deliveryRevenue};
        Object[] statReversals= {"TOTAL", "QUANTITY", reversalQuantity, "REVENUE", reversalRevenue};
        
        
        
        
        deliveryList.add(statDeliveries);
        reversalList.add(statReversals);
        
        String startDate = this.filter.getStartDate(deliveries);
        String endDate = this.filter.getEndDate(deliveries);
        
        summary.put("deliveries", deliveryList);
        summary.put("reversals", reversalList);
        summary.put("deliveryQuantity", deliveryQuantity);
        summary.put("deliveryRevenue", deliveryRevenue);
        summary.put("reversalQuantity", reversalQuantity);
        summary.put("reversalRevenue", reversalRevenue);
        summary.put("startDate", startDate);
        summary.put("endDate", endDate);
        
        return summary;
    }
    
    
    public HashMap getSummaryBy(HashMap criteria)throws FileNotFoundException, IOException, NoResultsException{
        HashMap summary = new HashMap();
        
        
        
        Set keys = criteria.keySet();
        for(Object item: keys){
            String key = item.toString();
            System.out.println("KEY: "+key);
            switch(key){
                case "Access Code":
                    String accessCode = criteria.get(key).toString();
                    System.out.println("Access Code: "+accessCode);
                    this.deliveriesFilter = this.deliveriesFilter.byAccessCode(accessCode);
                    this.reversalsFilter = this.reversalsFilter.byAccessCode(accessCode);                    
                    break;
                case "Date":
                    String date = criteria.get(key).toString();
                    System.out.println("Date: "+date);
                    this.deliveriesFilter = this.deliveriesFilter.byDate(date);
                    this.reversalsFilter = this.reversalsFilter.byDate(date);                    
                    break;
                case "Start Date":
                    String startDate = criteria.get("Start Date").toString();
                    String endDate = criteria.get("End Date").toString();
                    System.out.println("Date: "+startDate+" End: "+endDate);
                    this.deliveriesFilter = this.deliveriesFilter.byDateRange(startDate, endDate);
                    this.reversalsFilter = this.reversalsFilter.byDateRange(startDate, endDate);                    
                    break;
                case "Product ID":
                    String productID = criteria.get(key).toString();
                    System.out.println("Product ID: "+productID);
                    this.deliveriesFilter = this.deliveriesFilter.byProductID(productID);
                    this.reversalsFilter = this.reversalsFilter.byProductID(productID);                    
                    break;
                case "Product Name":
                    String productName = criteria.get(key).toString();
                    System.out.println("Product Name: "+productName);
                    this.deliveriesFilter = this.deliveriesFilter.byProductName(productName);
                    this.reversalsFilter = this.reversalsFilter.byProductName(productName);                    
                    break;
                case "Service Content Name":
                    String serviceContentName = criteria.get(key).toString();
                    System.out.println("Service Content Name: "+serviceContentName);
                    this.deliveriesFilter = this.deliveriesFilter.byServiceContentName(serviceContentName);
                    this.reversalsFilter = this.reversalsFilter.byServiceContentName(serviceContentName);                    
                    break;
                case "Service ID":
                    String serviceID = criteria.get(key).toString();
                    System.out.println("Service ID: "+serviceID);
                    this.deliveriesFilter = this.deliveriesFilter.byServiceID(serviceID);
                    this.reversalsFilter = this.reversalsFilter.byServiceID(serviceID);                    
                    break;
                default:
//                    this.deliveriesFilter = this.filter.getDeliveries();
//                    this.reversalsFilter = this.filter.getReversals();                   
                    break;
            }
            
        }
            
        
        List<RevenueStat> deliveries = deliveriesFilter.list();
        List<RevenueStat> reversals = reversalsFilter.list();
        System.out.println("DELIVERIES: "+deliveries.size());
        System.out.println("Reversals: "+reversals.size());
        if(deliveries.size()==0 && reversals.size()==0){
            throw new NoResultsException("No records");
        }
        
        
        List<Object[]> deliveryList = new ArrayList<Object[]>();
        List<Object[]> reversalList = new ArrayList<Object[]>();
        
        for(RevenueStat delivery: deliveries){
            Object[] statDelivery= 
                {
                    delivery.getDate(), 
                    delivery.getProductID(), 
                    delivery.getProductName(),
                    delivery.getAccessCode(),
                    delivery.getServiceContentName(),
                    delivery.getServiceID(),
                    delivery.getFee(),
                    delivery.getQuantity(),
                    delivery.getTotalRevenue()
                };
            
            deliveryList.add(statDelivery);
        }
        
        for(RevenueStat reversal: reversals){
            Object[] statReversal= 
                {
                    reversal.getDate(), 
                    reversal.getProductID(), 
                    reversal.getProductName(),
                    reversal.getAccessCode(),
                    reversal.getServiceContentName(),
                    reversal.getServiceID(),
                    reversal.getFee(),
                    reversal.getQuantity(),
                    reversal.getTotalRevenue()
                };
            
            reversalList.add(statReversal);
        }
        
        
        long deliveryQuantity = this.deliveriesFilter.getTotalQuantity(deliveries);
        long deliveryRevenue = this.deliveriesFilter.getTotalRevenue(deliveries);
        long reversalQuantity = this.reversalsFilter.getTotalQuantity(reversals);
        long reversalRevenue = this.reversalsFilter.getTotalRevenue(reversals);
        
        Object[] statDeliveries= {"TOTAL", "QUANTITY", deliveryQuantity, "REVENUE", deliveryRevenue};
        Object[] statReversals= {"TOTAL", "QUANTITY", reversalQuantity, "REVENUE", reversalRevenue};
        
        
        
        
        deliveryList.add(statDeliveries);
        reversalList.add(statReversals);
        
        String startDate = this.deliveriesFilter.getStartDate(deliveries);
        String endDate = this.deliveriesFilter.getEndDate(deliveries);
        
        summary.put("deliveries", deliveryList);
        summary.put("reversals", reversalList);
        summary.put("deliveryQuantity", deliveryQuantity);
        summary.put("deliveryRevenue", deliveryRevenue);
        summary.put("reversalQuantity", reversalQuantity);
        summary.put("reversalRevenue", reversalRevenue);
        summary.put("startDate", startDate);
        summary.put("endDate", endDate);
        
        return summary;
    }
    
    
    public List<RevenueStat> getDates()throws FileNotFoundException, IOException{
        List<RevenueStat> dates = this.filter.getDates(filter.getDeliveries().list());
        return dates;
    }
    
    public List<RevenueStat> getServiceIDs()throws FileNotFoundException, IOException{
        List<RevenueStat> serviceIDs = this.filter.getServiceIDs(filter.getDeliveries().list());
        return serviceIDs;
    }
    
    public List<RevenueStat> getServiceContentNames()throws FileNotFoundException, IOException{
        List<RevenueStat> serviceContentNames = this.filter.getServiceContentNames(filter.getDeliveries().list());
        return serviceContentNames;
    }
    
    public List<RevenueStat> getAccessCodes()throws FileNotFoundException, IOException{
        List<RevenueStat> accessCodes = this.filter.getAccessCodes(filter.getDeliveries().list());
        return accessCodes;
    }
    
    public List<RevenueStat> getProductIDs()throws FileNotFoundException, IOException{
        List<RevenueStat> productIDs = this.filter.getProductIDs(filter.getDeliveries().list());
        return productIDs;
    }
    
    public List<RevenueStat> getProductNames()throws FileNotFoundException, IOException{
        List<RevenueStat> productNames = this.filter.getProductNames(filter.getDeliveries().list());
        return productNames;
    }
    
    public static void main(String[] args){
        try{
            Analysis analysis = new Analysis("stats.csv");
//            analysis.getCummulativeSummary();
            HashMap criteria = new HashMap();
            
//            criteria.put("Date", "01/01/2018");
            criteria.put("Access Code", "22822");
            criteria.put("Product ID", "MDSP2000062732");
            criteria.put("Product Name", "22822_Christian_Service");
            criteria.put("Service ID", "6.01396E+15");
            criteria.put("Service Content Name", "221822_Christian_Service");
            
            analysis.getSummaryBy(criteria);
            
        }catch(NoResultsException nre){
            nre.printStackTrace();
        }catch(FileNotFoundException fnfe){
            fnfe.printStackTrace();
        }catch(IOException ioe){
            ioe.printStackTrace();
        }
    }
}
