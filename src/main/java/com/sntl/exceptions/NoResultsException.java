/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sntl.exceptions;

/**
 *
 * @author solunet
 */
public class NoResultsException extends Exception {
    public NoResultsException(String message){
        super(message);
    }
    public NoResultsException(Throwable throwable){
        super(throwable);
    }
    
}
