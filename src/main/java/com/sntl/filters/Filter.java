/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sntl.filters;

import com.sntl.models.RevenueStat;
import com.sntl.util.JSONConverter;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVRecord;

/**
 *
 * @author solunet
 */
public class Filter {
    private Reader csvFile;
    private String fileName;
    
    public Filter(String fileName)throws FileNotFoundException{
        this.getCSVFile(fileName);
    }
    
    public enum Headers{
        Date, 
        CPID, 
        CPName, 
        ServiceID, 
        ServiceContentName, 
        AccessCode, 
        ProductID, 
        ProductName, 
        ServiceType, 
        Fee, 
        Quantity,
        TotalRevenue
    }
    
    private Reader getCSVFile(String fileName)throws FileNotFoundException{
        this.fileName = this.fileName;
        File file = new File(fileName);
            
        InputStream in = new FileInputStream(fileName);
        this.csvFile = new BufferedReader(new InputStreamReader(in)); 
        
        //Alternative
        //Reader in = new FileReader("path/to/file.csv");
        
        return csvFile;
    }
    
    private List<RevenueStat> getRecords()throws IOException{
        Iterable<CSVRecord> csvRecords = CSVFormat.EXCEL.withSkipHeaderRecord().withHeader(Headers.class).parse(this.csvFile);
        List<RevenueStat> records = new ArrayList<>();
        
        
        for(Iterator itr=csvRecords.iterator(); itr.hasNext();){
            CSVRecord csvRecord = (CSVRecord)itr.next();
            if(itr.hasNext()){
                RevenueStat stat = new RevenueStat();
                stat.setDate(csvRecord.get(Headers.Date));
                stat.setCpID(csvRecord.get(Headers.CPID));
                stat.setCpName(csvRecord.get(Headers.CPName));
                stat.setServiceID(csvRecord.get(Headers.ServiceID));
                stat.setServiceContentName(csvRecord.get(Headers.ServiceContentName));
                stat.setAccessCode(csvRecord.get(Headers.AccessCode));
                stat.setProductID(csvRecord.get(Headers.ProductID));
                stat.setProductName(csvRecord.get(Headers.ProductName));
                stat.setServiceType(csvRecord.get(Headers.ServiceType));
                stat.setFee(Long.parseLong(csvRecord.get(Headers.Fee)));
                stat.setQuantity(Long.parseLong(csvRecord.get(Headers.Quantity)));
                stat.setTotalRevenue(Long.parseLong(csvRecord.get(Headers.TotalRevenue)));

                records.add(stat);
            }
        }
        return records;
    }
    
    public List<RevenueStat> getAll()throws FileNotFoundException, IOException{
        List<RevenueStat> records = this.getRecords();
        return records;
    }
    
    
    
    public List<RevenueStat> getDeliveries()throws FileNotFoundException, IOException{
        List<RevenueStat> records = this.getAll().stream().filter(stats->stats.fee>0).collect(Collectors.toList());
        return records;
    }
    
    public List<RevenueStat> getReversals()throws FileNotFoundException, IOException{
        List<RevenueStat> records = this.getAll().stream().filter(stats->stats.fee<0).collect(Collectors.toList());
        return records;
    }
    
    
    public List<RevenueStat> getDeliveriesByAccessCode(String accessCode)throws FileNotFoundException, IOException{
        List<RevenueStat> records = this.getAll().stream().filter(stats->stats.fee>0&&stats.accessCode.equals(accessCode)).collect(Collectors.toList());
        return records;
    }
    
    public List<RevenueStat> getReversalsByAccessCode(String accessCode)throws FileNotFoundException, IOException{
        List<RevenueStat> records = this.getAll().stream().filter(stats->stats.fee<0&&stats.accessCode.equals(accessCode)).collect(Collectors.toList());
        return records;
    }
    
    
    public List<RevenueStat> getDeliveriesByDate(String date)throws FileNotFoundException, IOException{
        List<RevenueStat> records = this.getAll().stream().filter(stats->stats.fee>0&&stats.date.equals(date)).collect(Collectors.toList());
        return records;
    }
    
    public List<RevenueStat> getReversalsByDate(String date)throws FileNotFoundException, IOException{
        List<RevenueStat> records = this.getAll().stream().filter(stats->stats.fee<0&&stats.date.equals(date)).collect(Collectors.toList());
        return records;
    }
    
    
    public List<RevenueStat> getDeliveriesByProductID(String productID)throws FileNotFoundException, IOException{
        List<RevenueStat> records = this.getAll().stream().filter(stats->stats.fee>0&&stats.productID.equals(productID)).collect(Collectors.toList());
        return records;
    }
    
    public List<RevenueStat> getReversalsByProductID(String productID)throws FileNotFoundException, IOException{
        List<RevenueStat> records = this.getAll().stream().filter(stats->stats.fee<0&&stats.productID.equals(productID)).collect(Collectors.toList());
        return records;
    }
    
    
    public List<RevenueStat> getDeliveriesByProductName(String productName)throws FileNotFoundException, IOException{
        List<RevenueStat> records = this.getAll().stream().filter(stats->stats.fee>0&&stats.productName.equals(productName)).collect(Collectors.toList());
        return records;
    }
    
    public List<RevenueStat> getReversalsByProductName(String productName)throws FileNotFoundException, IOException{
        List<RevenueStat> records = this.getAll().stream().filter(stats->stats.fee<0&&stats.productName.equals(productName)).collect(Collectors.toList());
        return records;
    }
    
    
    public List<RevenueStat> getDeliveriesByServiceContentName(String serviceContentName)throws FileNotFoundException, IOException{
        List<RevenueStat> records = this.getAll().stream().filter(stats->stats.fee>0&&stats.serviceContentName.equals(serviceContentName)).collect(Collectors.toList());
        return records;
    }
    
    public List<RevenueStat> getReversalsByServiceContentName(String serviceContentName)throws FileNotFoundException, IOException{
        List<RevenueStat> records = this.getAll().stream().filter(stats->stats.fee<0&&stats.serviceContentName.equals(serviceContentName)).collect(Collectors.toList());
        return records;
    }
    
    
    
    
    
    
    
    
    public static void main(String[] args){
        try{
            Filter filter = new Filter("stats.csv");
            List<RevenueStat> records = filter.getRecords();
            
            List<RevenueStat> filtered = records.stream().filter(stat->stat.accessCode.equals("22454")&&stat.fee>0).collect(Collectors.toList());
            
            
            
            
            String converted = JSONConverter.formatToJSON(filtered);
            System.out.println(converted);
            
        }catch(FileNotFoundException fnfe){
            System.out.println("FILE NOT FOUND \n"+fnfe.toString());
        }catch(IOException ioe){
            System.out.println("IOE \n"+ioe.toString());
        }catch(IllegalAccessException iae){
            System.out.println("IAE \n"+iae.toString());
        }
    }
    
    
    
}
