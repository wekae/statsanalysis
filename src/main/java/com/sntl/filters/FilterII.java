/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sntl.filters;

import com.sntl.models.RevenueStat;
import com.sntl.util.JSONConverter;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.LongSummaryStatistics;
import java.util.Iterator;
import java.util.List;
import java.util.LongSummaryStatistics;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVRecord;

/**
 *
 * @author solunet
 */
public class FilterII {
    
    private Reader csvFileReader;
    private File csvFile;
    private String fileName;
    private List<RevenueStat> records;
    private List<RevenueStat> filtered;
    
    public FilterII(String fileName)throws FileNotFoundException{
        this.getCSVFile(fileName);
    }
    public FilterII(File file)throws FileNotFoundException{
        this.getCSVFile(file);
    }
    
    public enum Headers{
        Date, 
        CPID, 
        CPName, 
        ServiceID, 
        ServiceContentName, 
        AccessCode, 
        ProductID, 
        ProductName, 
        ServiceType, 
        Fee, 
        Quantity,
        TotalRevenue
    }
    
    private Reader getCSVFile(String fileName)throws FileNotFoundException{
        
        this.fileName = fileName;
    
        InputStream in = new FileInputStream(fileName);
        this.csvFileReader = new BufferedReader(new InputStreamReader(in)); 
        
        //Alternative
        //Reader in = new FileReader(fileName);
        
        return this.csvFileReader;
    }
    
    private Reader getCSVFile(File file)throws FileNotFoundException{
        
        this.csvFile = file;

        InputStream in = new FileInputStream(this.csvFile);
        this.csvFileReader = new BufferedReader(new InputStreamReader(in)); 
        
        //Alternative
        //Reader in = new FileReader(fileName);
        
        return this.csvFileReader;
    }
    
    private List<RevenueStat> getRecords()throws IOException{
        List<RevenueStat> records = null;
        
        if(this.records == null){
            
            System.out.println("HERE");
        
            Iterable<CSVRecord> csvRecords = CSVFormat.EXCEL.withSkipHeaderRecord().withHeader(Headers.class).parse(this.csvFileReader);
            this.records = new ArrayList<>();


            for(Iterator itr=csvRecords.iterator(); itr.hasNext();){
                CSVRecord csvRecord = (CSVRecord)itr.next();
                if(itr.hasNext()){
                    RevenueStat stat = new RevenueStat();
                    stat.setDate(csvRecord.get(Headers.Date));
                    stat.setCpID(csvRecord.get(Headers.CPID));
                    stat.setCpName(csvRecord.get(Headers.CPName));
                    stat.setServiceID(csvRecord.get(Headers.ServiceID));
                    stat.setServiceContentName(csvRecord.get(Headers.ServiceContentName));
                    stat.setAccessCode(csvRecord.get(Headers.AccessCode));
                    stat.setProductID(csvRecord.get(Headers.ProductID));
                    stat.setProductName(csvRecord.get(Headers.ProductName));
                    stat.setServiceType(csvRecord.get(Headers.ServiceType));
//                    long feeVal = csvRecord.get(Headers.Fee).toString().equals("0.00")?0:Long.parseLong(csvRecord.get(Headers.Fee));
                    stat.setFee(Long.parseLong(csvRecord.get(Headers.Fee)));
//                    long qtyVal = csvRecord.get(Headers.Quantity).toString().equals("0.00")?0:Long.parseLong(csvRecord.get(Headers.Quantity));
                    stat.setQuantity(Long.parseLong(csvRecord.get(Headers.Quantity)));
//                    long ttlVal = csvRecord.get(Headers.TotalRevenue).toString().equals("0.00")?0:Long.parseLong(csvRecord.get(Headers.TotalRevenue));
                    stat.setTotalRevenue(Long.parseLong(csvRecord.get(Headers.TotalRevenue)));

                    this.records.add(stat);
                }
            }
            
            records = this.records;
        }else{
            System.out.println("HERRR");
            records = this.records;
        }
        return records;
    }
    
    private List<RevenueStat> getAll()throws FileNotFoundException, IOException{
        List<RevenueStat> records = this.getRecords();
        return records;
    }
    
    public List<RevenueStat> list(){
        return this.filtered;
    }
    
    public FilterII getDeliveries()throws FileNotFoundException, IOException{
        this.filtered = this.getAll().stream().filter(stats->stats.fee>0).collect(Collectors.toList());
        return this;
    }
    
    public FilterII getReversals()throws FileNotFoundException, IOException{
        this.filtered = this.getAll().stream().filter(stats->stats.fee<0).collect(Collectors.toList());
        return this;
    }
    
    public FilterII byAccessCode(String accessCode)throws FileNotFoundException, IOException{
        this.filtered = this.filtered.stream().filter(stats->stats.accessCode.equals(accessCode)).collect(Collectors.toList());
        return this;
    }
    
    public FilterII byDate(String date)throws FileNotFoundException, IOException{
        this.filtered = this.filtered.stream().filter(stats->stats.date.equals(date)).collect(Collectors.toList());
        return this;
    }
    
    public FilterII byDateRange(String startDate, String endDate)throws FileNotFoundException, IOException{
        DateTimeFormatter dateFormat = DateTimeFormatter.ofPattern("dd/MM/yyyy");
        this.filtered = this.filtered.stream().filter(stats->LocalDate.parse(stats.date,dateFormat).toEpochDay()>=LocalDate.parse(startDate,dateFormat).toEpochDay() && LocalDate.parse(stats.date,dateFormat).toEpochDay()<=LocalDate.parse(endDate,dateFormat).toEpochDay()).collect(Collectors.toList());
        return this;
    }
    
    public FilterII byProductID(String productID)throws FileNotFoundException, IOException{
        this.filtered = this.filtered.stream().filter(stats->stats.productID.equals(productID)).collect(Collectors.toList());
        return this;
    }
    
    public FilterII byProductName(String productName)throws FileNotFoundException, IOException{
        this.filtered = this.filtered.stream().filter(stats->stats.productName.equals(productName)).collect(Collectors.toList());
        return this;
    }
    
    public FilterII byServiceContentName(String serviceContentName)throws FileNotFoundException, IOException{
        this.filtered = this.filtered.stream().filter(stats->stats.serviceContentName.equals(serviceContentName)).collect(Collectors.toList());
        return this;
    }
    
    public FilterII byServiceID(String serviceID)throws FileNotFoundException, IOException{
        this.filtered = this.filtered.stream().filter(stats->stats.serviceID.equals(serviceID)).collect(Collectors.toList());
        return this;
    }
        
    public long getTotalQuantity(List<RevenueStat> filtered)throws FileNotFoundException, IOException{
        long totalQuantity = 0;
        LongSummaryStatistics quantityStats = filtered.stream().mapToLong(stats->stats.quantity).summaryStatistics();
        //Total
        totalQuantity = quantityStats.getSum();
        return totalQuantity;
    }
        
    public long getTotalRevenue(List<RevenueStat> filtered)throws FileNotFoundException, IOException{
        long totalRevenue = 0;
        LongSummaryStatistics quantityStats = filtered.stream().mapToLong(stats->stats.totalRevenue).summaryStatistics();
        //Total
        totalRevenue = quantityStats.getSum();
        return totalRevenue;
    }
        
    public String getStartDate(List<RevenueStat> filtered)throws FileNotFoundException, IOException{
        String startDate = "";
        DateTimeFormatter dateFormat = DateTimeFormatter.ofPattern("dd/MM/yyyy");
        LongSummaryStatistics quantityStats = filtered.stream().mapToLong(stats->LocalDate.parse(stats.date,dateFormat).toEpochDay()).summaryStatistics();
        //Total
        startDate = LocalDate.ofEpochDay(quantityStats.getMin()).toString();
        return startDate;
    }
    
    public String getEndDate(List<RevenueStat> filtered)throws FileNotFoundException, IOException{
        String endDate = "";
        DateTimeFormatter dateFormat = DateTimeFormatter.ofPattern("dd/MM/yyyy");
        LongSummaryStatistics quantityStats = filtered.stream().mapToLong(stats->LocalDate.parse(stats.date,dateFormat).toEpochDay()).summaryStatistics();
        //Total
        endDate = LocalDate.ofEpochDay(quantityStats.getMax()).toString();
        return endDate;
    }
    
    public List<RevenueStat> getDates(List<RevenueStat> filtered)throws FileNotFoundException, IOException{
        List dates = this.filtered.stream().filter(distinctByKey(RevenueStat::getDate)).collect(Collectors.toList());
        return dates;
    }
    
    public List<RevenueStat> getServiceIDs(List<RevenueStat> filtered)throws FileNotFoundException, IOException{
        List dates = this.filtered.stream().filter(distinctByKey(RevenueStat::getServiceID)).collect(Collectors.toList());
        return dates;
    }
    
    public List<RevenueStat> getServiceContentNames(List<RevenueStat> filtered)throws FileNotFoundException, IOException{
        List dates = this.filtered.stream().filter(distinctByKey(RevenueStat::getServiceContentName)).collect(Collectors.toList());
        return dates;
    }
    
    public List<RevenueStat> getAccessCodes(List<RevenueStat> filtered)throws FileNotFoundException, IOException{
        List dates = this.filtered.stream().filter(distinctByKey(RevenueStat::getAccessCode)).collect(Collectors.toList());
        return dates;
    }
    
    public List<RevenueStat> getProductIDs(List<RevenueStat> filtered)throws FileNotFoundException, IOException{
        List dates = this.filtered.stream().filter(distinctByKey(RevenueStat::getProductID)).collect(Collectors.toList());
        return dates;
    }
    
    public List<RevenueStat> getProductNames(List<RevenueStat> filtered)throws FileNotFoundException, IOException{
        List dates = this.filtered.stream().filter(distinctByKey(RevenueStat::getProductName)).collect(Collectors.toList());
        return dates;
    }
    
    
    
    public static <T> Predicate<T> distinctByKey(Function<? super T, ?> keyExtractor){
        Set<Object> seen = ConcurrentHashMap.newKeySet();
        return t->seen.add(keyExtractor.apply(t));
    }
    
    
    
    
    
    
    public static void main(String[] args){
        try{
            FilterII filter = new FilterII("stats.csv");
            
            List<RevenueStat> filtered1 = filter.getDeliveries().byAccessCode("22454").list();
            List<RevenueStat> filtered2 = filter.getReversals().byAccessCode("22454").list();
            List<RevenueStat> filtered3 = filter.getDeliveries().byAccessCode("22455").list();
            List<RevenueStat> filtered4 = filter.getReversals().byAccessCode("22455").list();
            List<RevenueStat> filtered5 = filter.getDeliveries().byAccessCode("22822").list();
            List<RevenueStat> filtered6 = filter.getReversals().byAccessCode("22822").list();
            List<RevenueStat> filtered7 = filter.getDeliveries().byAccessCode("22899").list();
            List<RevenueStat> filtered8 = filter.getReversals().byAccessCode("22899").list();
            List<RevenueStat> filtered9 = filter.getDeliveries().byAccessCode("20303").list();
            List<RevenueStat> filtered10 = filter.getReversals().byAccessCode("20303").list();
            
            
            String converted = JSONConverter.formatToJSON(filtered1);
            String converted2 = JSONConverter.formatToJSON(filtered2);
//            System.out.println(converted);
//            System.out.println("\n\n\n");
//            System.out.println(converted2);


            System.out.println("TOTAL QUANTITY 22454: "+filter.getTotalQuantity(filtered1));
            System.out.println("TOTAL REVENUE 22454: "+filter.getTotalRevenue(filtered1));
            System.out.println("TOTAL QUANTITY 22454: "+filter.getTotalQuantity(filtered2));
            System.out.println("TOTAL REVENUE 22454: "+filter.getTotalRevenue(filtered2));
            System.out.println("\n");

            System.out.println("TOTAL QUANTITY 22455: "+filter.getTotalQuantity(filtered3));
            System.out.println("TOTAL REVENUE 22455: "+filter.getTotalRevenue(filtered3));
            System.out.println("TOTAL QUANTITY 22455: "+filter.getTotalQuantity(filtered4));
            System.out.println("TOTAL REVENUE 22455: "+filter.getTotalRevenue(filtered4));
            System.out.println("\n");

            System.out.println("TOTAL QUANTITY 22822: "+filter.getTotalQuantity(filtered5));
            System.out.println("TOTAL REVENUE 22822: "+filter.getTotalRevenue(filtered5));
            System.out.println("TOTAL QUANTITY 22822: "+filter.getTotalQuantity(filtered6));
            System.out.println("TOTAL REVENUE 22822: "+filter.getTotalRevenue(filtered6));
            System.out.println("\n");

            System.out.println("TOTAL QUANTITY 22899: "+filter.getTotalQuantity(filtered7));
            System.out.println("TOTAL REVENUE 22899: "+filter.getTotalRevenue(filtered7));
            System.out.println("TOTAL QUANTITY 22899: "+filter.getTotalQuantity(filtered8));
            System.out.println("TOTAL REVENUE 22899: "+filter.getTotalRevenue(filtered8));
            System.out.println("\n");

            System.out.println("TOTAL QUANTITY 20303: "+filter.getTotalQuantity(filtered9));
            System.out.println("TOTAL REVENUE 20303: "+filter.getTotalRevenue(filtered9));
            System.out.println("TOTAL QUANTITY 20303: "+filter.getTotalQuantity(filtered10));
            System.out.println("TOTAL REVENUE 20303: "+filter.getTotalRevenue(filtered10));
            System.out.println("\n");
            
            System.out.println("START DATE: "+filter.getStartDate(filtered10));
            System.out.println("END DATE: "+filter.getEndDate(filtered10));
            
            System.out.println("\n DATES: \n");
            for(Object date: filter.getDates(filtered9)){
                System.out.println("Date: "+date.toString());
            }
            
        }catch(FileNotFoundException fnfe){
            System.out.println("FILE NOT FOUND \n"+fnfe.toString());
        }catch(IOException ioe){
            System.out.println("IOE \n"+ioe.toString());
        }catch(IllegalAccessException iae){
            System.out.println("IAE \n"+iae.toString());
        }
    }
    
    
    
}
