/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sntl.filters;

import java.io.File;
import javax.swing.filechooser.FileFilter;

/**
 *
 * @author solunet
 */
public class CSVFileFIlterII extends FileFilter{
    private final String[] okFileExtensions = new String[]{"csv"};
    
    public boolean accept(File file){
        for(String extension: okFileExtensions){
            if (file.isDirectory()) {
                return true;
            }
            if(file.getName().toLowerCase().endsWith(extension)){
                return true;
            }
        }
        return false;
    }
    
    public String getDescription(){
        return "CSV File";
    }
    
}
