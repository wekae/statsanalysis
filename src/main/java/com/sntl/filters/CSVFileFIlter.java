/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sntl.filters;

import java.io.File;
import java.io.FileFilter;

/**
 *
 * @author solunet
 */
public class CSVFileFIlter implements FileFilter{
    private final String[] okFileExtensions = new String[]{"csv"};
    
    public boolean accept(File file){
        for(String extension: okFileExtensions){
            if(file.getName().toLowerCase().endsWith(extension)){
                return true;
            }
        }
        return false;
    }
    
}
